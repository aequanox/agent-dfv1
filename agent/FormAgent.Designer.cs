﻿namespace Switrace
{
    partial class FormAgent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormAgent));
            this.ReadWriteThread = new System.ComponentModel.BackgroundWorker();
            this.FormUpdateTimer = new System.Windows.Forms.Timer(this.components);
            this.ANxVoltageToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.version_lbl = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.measures_txtbx = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.sampletime_txtbx = new System.Windows.Forms.TextBox();
            this.serial_lbl = new System.Windows.Forms.Label();
            this.serial_txtbx = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.StartDelay_txtbx = new System.Windows.Forms.TextBox();
            this.groupSettings = new System.Windows.Forms.GroupBox();
            this.groupAlarms = new System.Windows.Forms.GroupBox();
            this.tempUnit_txtbx = new System.Windows.Forms.TextBox();
            this.tempUnit_lbl = new System.Windows.Forms.Label();
            this.LLAlert_lbl = new System.Windows.Forms.Label();
            this.chart_h = new System.Windows.Forms.TextBox();
            this.LAlert_lbl = new System.Windows.Forms.Label();
            this.HAlert_lbl = new System.Windows.Forms.Label();
            this.chart_hh = new System.Windows.Forms.TextBox();
            this.HHAlert_lbl = new System.Windows.Forms.Label();
            this.chart_ll = new System.Windows.Forms.TextBox();
            this.chart_l = new System.Windows.Forms.TextBox();
            this.groupComments = new System.Windows.Forms.GroupBox();
            this.SecondRow_txtbx = new System.Windows.Forms.TextBox();
            this.FirstRow_txtbx = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.myNotifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.closeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.StatusBox_lbl = new System.Windows.Forms.Label();
            this.SystemStatus_lbl = new System.Windows.Forms.Label();
            this.serverStatus_txtbx = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            //this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            //this.checkForUpdatesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.statusLabel = new Switrace.StatusLabel();
            this.groupSettings.SuspendLayout();
            this.groupAlarms.SuspendLayout();
            this.groupComments.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ReadWriteThread
            // 
            this.ReadWriteThread.WorkerReportsProgress = true;
            this.ReadWriteThread.DoWork += new System.ComponentModel.DoWorkEventHandler(this.ReadWriteThread_DoWork);
            // 
            // FormUpdateTimer
            // 
            this.FormUpdateTimer.Enabled = true;
            this.FormUpdateTimer.Interval = 6;
            this.FormUpdateTimer.Tick += new System.EventHandler(this.FormUpdateTimer_Tick);
            // 
            // ANxVoltageToolTip
            // 
            this.ANxVoltageToolTip.AutomaticDelay = 20;
            this.ANxVoltageToolTip.AutoPopDelay = 20000;
            this.ANxVoltageToolTip.InitialDelay = 15;
            this.ANxVoltageToolTip.ReshowDelay = 15;
            // 
            // version_lbl
            // 
            this.version_lbl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.version_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.version_lbl.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.version_lbl.Location = new System.Drawing.Point(-1, 455);
            this.version_lbl.Name = "version_lbl";
            this.version_lbl.Size = new System.Drawing.Size(338, 12);
            this.version_lbl.TabIndex = 134;
            this.version_lbl.Text = "Dataflow Agent - Switrace SA";
            this.version_lbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.TabIndex = 49;
            this.label1.Text = "Measures";
            // 
            // measures_txtbx
            // 
            this.measures_txtbx.BackColor = System.Drawing.Color.White;
            this.measures_txtbx.Location = new System.Drawing.Point(83, 46);
            this.measures_txtbx.Name = "measures_txtbx";
            this.measures_txtbx.ReadOnly = true;
            this.measures_txtbx.Size = new System.Drawing.Size(58, 20);
            this.measures_txtbx.TabIndex = 10;
            this.measures_txtbx.Text = "----";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(160, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 13);
            this.label2.TabIndex = 51;
            this.label2.Text = "Sample Time";
            // 
            // sampletime_txtbx
            // 
            this.sampletime_txtbx.BackColor = System.Drawing.Color.White;
            this.sampletime_txtbx.Location = new System.Drawing.Point(239, 49);
            this.sampletime_txtbx.Name = "sampletime_txtbx";
            this.sampletime_txtbx.ReadOnly = true;
            this.sampletime_txtbx.Size = new System.Drawing.Size(55, 20);
            this.sampletime_txtbx.TabIndex = 20;
            this.sampletime_txtbx.Text = "----";
            // 
            // serial_lbl
            // 
            this.serial_lbl.AutoSize = true;
            this.serial_lbl.Location = new System.Drawing.Point(8, 21);
            this.serial_lbl.Name = "serial_lbl";
            this.serial_lbl.Size = new System.Drawing.Size(73, 13);
            this.serial_lbl.TabIndex = 102;
            this.serial_lbl.Text = "Serial Number";
            // 
            // serial_txtbx
            // 
            this.serial_txtbx.BackColor = System.Drawing.Color.White;
            this.serial_txtbx.Location = new System.Drawing.Point(83, 18);
            this.serial_txtbx.MaxLength = 100;
            this.serial_txtbx.Name = "serial_txtbx";
            this.serial_txtbx.ReadOnly = true;
            this.serial_txtbx.Size = new System.Drawing.Size(82, 20);
            this.serial_txtbx.TabIndex = 103;
            this.serial_txtbx.Text = "Serial Number";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 75);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 13);
            this.label5.TabIndex = 104;
            this.label5.Text = "Start delay (s)";
            // 
            // StartDelay_txtbx
            // 
            this.StartDelay_txtbx.BackColor = System.Drawing.Color.White;
            this.StartDelay_txtbx.Location = new System.Drawing.Point(83, 72);
            this.StartDelay_txtbx.MaxLength = 15;
            this.StartDelay_txtbx.Name = "StartDelay_txtbx";
            this.StartDelay_txtbx.ReadOnly = true;
            this.StartDelay_txtbx.Size = new System.Drawing.Size(82, 20);
            this.StartDelay_txtbx.TabIndex = 105;
            // 
            // groupSettings
            // 
            this.groupSettings.Controls.Add(this.StartDelay_txtbx);
            this.groupSettings.Controls.Add(this.label5);
            this.groupSettings.Controls.Add(this.serial_txtbx);
            this.groupSettings.Controls.Add(this.serial_lbl);
            this.groupSettings.Controls.Add(this.sampletime_txtbx);
            this.groupSettings.Controls.Add(this.label2);
            this.groupSettings.Controls.Add(this.measures_txtbx);
            this.groupSettings.Controls.Add(this.label1);
            this.groupSettings.Enabled = false;
            this.groupSettings.Location = new System.Drawing.Point(12, 158);
            this.groupSettings.Name = "groupSettings";
            this.groupSettings.Size = new System.Drawing.Size(310, 108);
            this.groupSettings.TabIndex = 48;
            this.groupSettings.TabStop = false;
            this.groupSettings.Enter += new System.EventHandler(this.groupSettings_Enter);
            // 
            // groupAlarms
            // 
            this.groupAlarms.Controls.Add(this.tempUnit_txtbx);
            this.groupAlarms.Controls.Add(this.tempUnit_lbl);
            this.groupAlarms.Controls.Add(this.LLAlert_lbl);
            this.groupAlarms.Controls.Add(this.chart_h);
            this.groupAlarms.Controls.Add(this.LAlert_lbl);
            this.groupAlarms.Controls.Add(this.HAlert_lbl);
            this.groupAlarms.Controls.Add(this.chart_hh);
            this.groupAlarms.Controls.Add(this.HHAlert_lbl);
            this.groupAlarms.Controls.Add(this.chart_ll);
            this.groupAlarms.Controls.Add(this.chart_l);
            this.groupAlarms.Enabled = false;
            this.groupAlarms.Location = new System.Drawing.Point(12, 337);
            this.groupAlarms.Margin = new System.Windows.Forms.Padding(0);
            this.groupAlarms.Name = "groupAlarms";
            this.groupAlarms.Size = new System.Drawing.Size(310, 106);
            this.groupAlarms.TabIndex = 135;
            this.groupAlarms.TabStop = false;
            // 
            // tempUnit_txtbx
            // 
            this.tempUnit_txtbx.BackColor = System.Drawing.Color.White;
            this.tempUnit_txtbx.Location = new System.Drawing.Point(83, 18);
            this.tempUnit_txtbx.Name = "tempUnit_txtbx";
            this.tempUnit_txtbx.ReadOnly = true;
            this.tempUnit_txtbx.Size = new System.Drawing.Size(214, 20);
            this.tempUnit_txtbx.TabIndex = 120;
            // 
            // tempUnit_lbl
            // 
            this.tempUnit_lbl.AutoSize = true;
            this.tempUnit_lbl.Location = new System.Drawing.Point(9, 21);
            this.tempUnit_lbl.Name = "tempUnit_lbl";
            this.tempUnit_lbl.Size = new System.Drawing.Size(54, 13);
            this.tempUnit_lbl.TabIndex = 119;
            this.tempUnit_lbl.Text = "Temp unit";
            this.tempUnit_lbl.Click += new System.EventHandler(this.tempUnit_lbl_Click);
            // 
            // LLAlert_lbl
            // 
            this.LLAlert_lbl.AutoSize = true;
            this.LLAlert_lbl.ForeColor = System.Drawing.Color.Blue;
            this.LLAlert_lbl.Location = new System.Drawing.Point(159, 75);
            this.LLAlert_lbl.Name = "LLAlert_lbl";
            this.LLAlert_lbl.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LLAlert_lbl.Size = new System.Drawing.Size(43, 13);
            this.LLAlert_lbl.TabIndex = 72;
            this.LLAlert_lbl.Text = "LL Alert";
            // 
            // chart_h
            // 
            this.chart_h.BackColor = System.Drawing.Color.White;
            this.chart_h.Location = new System.Drawing.Point(82, 46);
            this.chart_h.Name = "chart_h";
            this.chart_h.ReadOnly = true;
            this.chart_h.Size = new System.Drawing.Size(57, 20);
            this.chart_h.TabIndex = 69;
            // 
            // LAlert_lbl
            // 
            this.LAlert_lbl.AutoSize = true;
            this.LAlert_lbl.ForeColor = System.Drawing.Color.Navy;
            this.LAlert_lbl.Location = new System.Drawing.Point(159, 49);
            this.LAlert_lbl.Name = "LAlert_lbl";
            this.LAlert_lbl.Size = new System.Drawing.Size(37, 13);
            this.LAlert_lbl.TabIndex = 70;
            this.LAlert_lbl.Text = "L Alert";
            // 
            // HAlert_lbl
            // 
            this.HAlert_lbl.AutoSize = true;
            this.HAlert_lbl.ForeColor = System.Drawing.Color.Maroon;
            this.HAlert_lbl.Location = new System.Drawing.Point(9, 49);
            this.HAlert_lbl.Name = "HAlert_lbl";
            this.HAlert_lbl.Size = new System.Drawing.Size(39, 13);
            this.HAlert_lbl.TabIndex = 68;
            this.HAlert_lbl.Text = "H Alert";
            // 
            // chart_hh
            // 
            this.chart_hh.BackColor = System.Drawing.Color.White;
            this.chart_hh.Location = new System.Drawing.Point(82, 72);
            this.chart_hh.Name = "chart_hh";
            this.chart_hh.ReadOnly = true;
            this.chart_hh.Size = new System.Drawing.Size(57, 20);
            this.chart_hh.TabIndex = 67;
            // 
            // HHAlert_lbl
            // 
            this.HHAlert_lbl.AutoSize = true;
            this.HHAlert_lbl.ForeColor = System.Drawing.Color.Red;
            this.HHAlert_lbl.Location = new System.Drawing.Point(9, 75);
            this.HHAlert_lbl.Name = "HHAlert_lbl";
            this.HHAlert_lbl.Size = new System.Drawing.Size(44, 13);
            this.HHAlert_lbl.TabIndex = 66;
            this.HHAlert_lbl.Text = "HHAlert";
            // 
            // chart_ll
            // 
            this.chart_ll.BackColor = System.Drawing.Color.White;
            this.chart_ll.Location = new System.Drawing.Point(240, 72);
            this.chart_ll.Name = "chart_ll";
            this.chart_ll.ReadOnly = true;
            this.chart_ll.Size = new System.Drawing.Size(57, 20);
            this.chart_ll.TabIndex = 73;
            // 
            // chart_l
            // 
            this.chart_l.BackColor = System.Drawing.Color.White;
            this.chart_l.Location = new System.Drawing.Point(240, 46);
            this.chart_l.Name = "chart_l";
            this.chart_l.ReadOnly = true;
            this.chart_l.Size = new System.Drawing.Size(57, 20);
            this.chart_l.TabIndex = 71;
            // 
            // groupComments
            // 
            this.groupComments.Controls.Add(this.SecondRow_txtbx);
            this.groupComments.Controls.Add(this.FirstRow_txtbx);
            this.groupComments.Controls.Add(this.label3);
            this.groupComments.Controls.Add(this.label4);
            this.groupComments.Enabled = false;
            this.groupComments.Location = new System.Drawing.Point(12, 269);
            this.groupComments.Margin = new System.Windows.Forms.Padding(0);
            this.groupComments.Name = "groupComments";
            this.groupComments.Size = new System.Drawing.Size(311, 68);
            this.groupComments.TabIndex = 136;
            this.groupComments.TabStop = false;
            // 
            // SecondRow_txtbx
            // 
            this.SecondRow_txtbx.BackColor = System.Drawing.Color.White;
            this.SecondRow_txtbx.Location = new System.Drawing.Point(85, 41);
            this.SecondRow_txtbx.MaxLength = 15;
            this.SecondRow_txtbx.Name = "SecondRow_txtbx";
            this.SecondRow_txtbx.ReadOnly = true;
            this.SecondRow_txtbx.Size = new System.Drawing.Size(211, 20);
            this.SecondRow_txtbx.TabIndex = 84;
            // 
            // FirstRow_txtbx
            // 
            this.FirstRow_txtbx.BackColor = System.Drawing.Color.White;
            this.FirstRow_txtbx.Location = new System.Drawing.Point(85, 15);
            this.FirstRow_txtbx.MaxLength = 15;
            this.FirstRow_txtbx.Name = "FirstRow_txtbx";
            this.FirstRow_txtbx.ReadOnly = true;
            this.FirstRow_txtbx.Size = new System.Drawing.Size(211, 20);
            this.FirstRow_txtbx.TabIndex = 83;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 18);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 13);
            this.label3.TabIndex = 81;
            this.label3.Text = "First Row";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 44);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 13);
            this.label4.TabIndex = 82;
            this.label4.Text = "Second Row";
            // 
            // myNotifyIcon
            // 
            this.myNotifyIcon.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.myNotifyIcon.ContextMenuStrip = this.contextMenuStrip1;
            this.myNotifyIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("myNotifyIcon.Icon")));
            this.myNotifyIcon.Text = "Dataflow Agent";
            this.myNotifyIcon.Visible = true;
            this.myNotifyIcon.DoubleClick += new System.EventHandler(this.myNotifyIcon_DoubleClick);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.closeToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(98, 26);
            // 
            // closeToolStripMenuItem
            // 
            this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
            this.closeToolStripMenuItem.Size = new System.Drawing.Size(97, 22);
            this.closeToolStripMenuItem.Text = "Quit";
            this.closeToolStripMenuItem.Click += new System.EventHandler(this.closeToolStripMenuItem_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.StatusBox_lbl);
            this.groupBox1.Controls.Add(this.SystemStatus_lbl);
            this.groupBox1.Location = new System.Drawing.Point(12, 58);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(310, 40);
            this.groupBox1.TabIndex = 143;
            this.groupBox1.TabStop = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(199, 14);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(43, 13);
            this.label8.TabIndex = 146;
            this.label8.Text = "Status :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(4, 14);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(70, 13);
            this.label7.TabIndex = 145;
            this.label7.Text = "Device type :";
            // 
            // StatusBox_lbl
            // 
            this.StatusBox_lbl.AutoSize = true;
            this.StatusBox_lbl.BackColor = System.Drawing.SystemColors.Control;
            this.StatusBox_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StatusBox_lbl.ForeColor = System.Drawing.Color.Teal;
            this.StatusBox_lbl.Location = new System.Drawing.Point(77, 14);
            this.StatusBox_lbl.Name = "StatusBox_lbl";
            this.StatusBox_lbl.Size = new System.Drawing.Size(67, 13);
            this.StatusBox_lbl.TabIndex = 143;
            this.StatusBox_lbl.Text = "not detected";
            this.StatusBox_lbl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // SystemStatus_lbl
            // 
            this.SystemStatus_lbl.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.SystemStatus_lbl.Location = new System.Drawing.Point(241, 8);
            this.SystemStatus_lbl.Name = "SystemStatus_lbl";
            this.SystemStatus_lbl.Size = new System.Drawing.Size(69, 25);
            this.SystemStatus_lbl.TabIndex = 144;
            this.SystemStatus_lbl.Text = "Unknown";
            this.SystemStatus_lbl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // serverStatus_txtbx
            // 
            this.serverStatus_txtbx.BackColor = System.Drawing.Color.Khaki;
            this.serverStatus_txtbx.Location = new System.Drawing.Point(12, 31);
            this.serverStatus_txtbx.Name = "serverStatus_txtbx";
            this.serverStatus_txtbx.Size = new System.Drawing.Size(310, 24);
            this.serverStatus_txtbx.TabIndex = 145;
            this.serverStatus_txtbx.Text = "Server status";
            this.serverStatus_txtbx.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            //this.fileToolStripMenuItem,
            this.exitToolStripMenuItem1});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(6, 4, 0, 2);
            this.menuStrip1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.menuStrip1.Size = new System.Drawing.Size(339, 25);
            this.menuStrip1.TabIndex = 147;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            //this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            //this.checkForUpdatesToolStripMenuItem});
            //this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            //this.fileToolStripMenuItem.RightToLeft = System.Windows.Forms.RightToLeft.No;
            //this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 19);
            //this.fileToolStripMenuItem.Text = "File";
            // 
            // checkForUpdatesToolStripMenuItem
            // 
            //this.checkForUpdatesToolStripMenuItem.Name = "checkForUpdatesToolStripMenuItem";
            //this.checkForUpdatesToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            //this.checkForUpdatesToolStripMenuItem.Text = "Check for updates";
            //this.checkForUpdatesToolStripMenuItem.Click += new System.EventHandler(this.updateTask);
            // 
            // exitToolStripMenuItem1
            // 
            this.exitToolStripMenuItem1.Name = "exitToolStripMenuItem1";
            this.exitToolStripMenuItem1.Size = new System.Drawing.Size(37, 19);
            this.exitToolStripMenuItem1.Text = "Exit";
            this.exitToolStripMenuItem1.Click += new System.EventHandler(this.closeToolStripMenuItem_Click);
            // 
            // statusLabel
            // 
            this.statusLabel.Location = new System.Drawing.Point(12, 104);
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(310, 48);
            this.statusLabel.TabIndex = 141;
            // 
            // FormAgent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(339, 475);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.serverStatus_txtbx);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.statusLabel);
            this.Controls.Add(this.version_lbl);
            this.Controls.Add(this.groupAlarms);
            this.Controls.Add(this.groupSettings);
            this.Controls.Add(this.groupComments);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(355, 358);
            this.Name = "FormAgent";
            this.ShowInTaskbar = false;
            this.Text = "Dataflow Agent";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Resize += new System.EventHandler(this.Form1_Resize);
            this.groupSettings.ResumeLayout(false);
            this.groupSettings.PerformLayout();
            this.groupAlarms.ResumeLayout(false);
            this.groupAlarms.PerformLayout();
            this.groupComments.ResumeLayout(false);
            this.groupComments.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.ComponentModel.BackgroundWorker ReadWriteThread;
        private System.Windows.Forms.Timer FormUpdateTimer;
        private System.Windows.Forms.ToolTip ANxVoltageToolTip;
        //private System.Windows.Forms.Button GetTemp_btn;
        private System.Windows.Forms.Label version_lbl;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox measures_txtbx;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox sampletime_txtbx;
        private System.Windows.Forms.Label serial_lbl;
        private System.Windows.Forms.TextBox serial_txtbx;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox StartDelay_txtbx;
        private System.Windows.Forms.GroupBox groupSettings;
        private System.Windows.Forms.GroupBox groupAlarms;
        private System.Windows.Forms.Label LLAlert_lbl;
        private System.Windows.Forms.TextBox chart_h;
        private System.Windows.Forms.Label LAlert_lbl;
        private System.Windows.Forms.Label HAlert_lbl;
        private System.Windows.Forms.TextBox chart_hh;
        private System.Windows.Forms.Label HHAlert_lbl;
        private System.Windows.Forms.TextBox chart_ll;
        private System.Windows.Forms.TextBox chart_l;
        private System.Windows.Forms.GroupBox groupComments;
        private System.Windows.Forms.TextBox SecondRow_txtbx;
        private System.Windows.Forms.TextBox FirstRow_txtbx;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        //private System.Windows.Forms.Button chart_btn;
        private System.Windows.Forms.Label tempUnit_lbl;
        private System.Windows.Forms.TextBox tempUnit_txtbx;
        private StatusLabel statusLabel;
        private System.Windows.Forms.NotifyIcon myNotifyIcon;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label StatusBox_lbl;
        private System.Windows.Forms.Label SystemStatus_lbl;
        private System.Windows.Forms.Label serverStatus_txtbx;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItem;
        private System.Windows.Forms.MenuStrip menuStrip1;
        //private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        //private System.Windows.Forms.ToolStripMenuItem checkForUpdatesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem1;
    }
}

