﻿using System;
using System.Text;
using System.Security.Cryptography;
using System.IO;
using System.Diagnostics;

namespace Switrace
{
    public class RSAKeyValue
    {
        public byte[] Modulus { get; set; }
        public byte[] Exponent { get; set; }
    }

    class Security
    {
        //private static RSAParameters clientPubKey;
        private static string _ClientPublicKey = null;
        //private static string _SharedKey = null;

        public static string SharedKey
        {
            get;
            private set;
        } = "";



        public static void SetClientPublicKey(string publickey_base64)
        {
            byte[] publickey = Convert.FromBase64String(publickey_base64);
            _ClientPublicKey = Encoding.UTF8.GetString(publickey);
        }



        public static RSAParameters ParseRSAPArameters(string key)
        {
            //get a stream from the string
            var sr = new StringReader(key);
            //we need a deserializer
            var xs = new System.Xml.Serialization.XmlSerializer(typeof(RSAKeyValue));

            RSAKeyValue kv = (RSAKeyValue)xs.Deserialize(sr);

            RSAParameters publicKey = new RSAParameters
            {
                Modulus = kv.Modulus,
                Exponent = kv.Exponent
            };

            return publicKey;
        }

        


        public static string CreateSharedKey()
        {
            SharedKey = System.Web.Security.Membership.GeneratePassword(20, 5);
            return SharedKey;
        }



        // Encode message using simmetric algorithm
        public static string ProtectMessage(string msg)
        {
            return Security.EncodeAES(msg);
        }




        // Asymmetric encryption - Used to encrypt the symmetric key
        public static byte[] EncryptRSA(byte[] data)
        {
            RSAParameters pubkey  = ParseRSAPArameters(_ClientPublicKey);
            RSACryptoServiceProvider csp = new RSACryptoServiceProvider();
            csp.ImportParameters(pubkey);

            //apply pkcs#1.5 padding and encrypt our data 
            byte[] bytesCypherText = csp.Encrypt(data, false);

            //we might want a string representation of our cypher text... base64 will do
            return bytesCypherText;
        }



        // Symmetric encryption - Used to encrypt the main data/body
        public static string EncodeAES(string message)
        {
            byte[] encodedBytes = null;

            using (AesManaged cypher = new AesManaged())
            {
                byte[] data = Encoding.ASCII.GetBytes(message);

                cypher.GenerateIV();
                cypher.Mode = CipherMode.CBC;
                cypher.Padding = PaddingMode.PKCS7;
                cypher.KeySize = 256;

                byte[] salt = new byte[16];
                RNGCryptoServiceProvider saltGenerator = new RNGCryptoServiceProvider();
                saltGenerator.GetBytes(salt);

                byte[] key = Encoding.ASCII.GetBytes(SharedKey);

                Rfc2898DeriveBytes PBKDF2 = new Rfc2898DeriveBytes(key, salt, 1000);
                byte[] rgbKey = PBKDF2.GetBytes(32);


                ICryptoTransform cipher = cypher.CreateEncryptor(rgbKey, cypher.IV);

                using (MemoryStream memoryStream = new MemoryStream())
                {
                    using (CryptoStream cryptoStream = new CryptoStream(memoryStream, cipher, CryptoStreamMode.Write))
                    {
                        cryptoStream.Write(data, 0, data.Length);
                        cryptoStream.Close();
                    }

                    encodedBytes = memoryStream.ToArray();

                    //Trace.WriteLine("IV " + Convert.ToBase64String(cypher.IV));
                    //Trace.WriteLine("SALT " + Convert.ToBase64String(salt));
                    //Trace.WriteLine("KEY " + Convert.ToBase64String(key));
                    //Trace.WriteLine("RGBKEY " + Convert.ToBase64String(rgbKey));
                    //Trace.WriteLine("ENCODED " + Convert.ToBase64String(encodedBytes));
                }

                using (MemoryStream memoryStream2 = new MemoryStream())
                {
                    memoryStream2.Write(cypher.IV, 0, cypher.IV.Length);
                    memoryStream2.Write(salt, 0, salt.Length);
                    memoryStream2.Write(encodedBytes, 0, encodedBytes.Length);
                    var arr = memoryStream2.ToArray();
                    return Convert.ToBase64String(memoryStream2.ToArray());
                }
            }

        }



        public static string DecodeAES(string message_b64)
        {
            byte[] data = Convert.FromBase64String(message_b64);

            using (AesManaged cypher = new AesManaged())
            {
                cypher.Mode = CipherMode.CBC;
                cypher.Padding = PaddingMode.PKCS7;
                cypher.KeySize = 256;

                byte[] iv = new byte[cypher.IV.Length];
                byte[] saltBuffer = new byte[16];
                byte[] extract = new byte[data.Length - saltBuffer.Length - iv.Length];
                byte[] output = new byte[extract.Length];
                //SharedKey = "password";
                byte[] key = Encoding.ASCII.GetBytes(SharedKey);

                // Extract IV
                Array.Copy(data, 0, iv, 0, iv.Length);
                // Extract salt
                Array.Copy(data, iv.Length, saltBuffer, 0, saltBuffer.Length);
                // Extract encrypted bytes
                Array.Copy(data, iv.Length + saltBuffer.Length, extract, 0, extract.Length);

                Rfc2898DeriveBytes PBKDF2 = new Rfc2898DeriveBytes(key, saltBuffer, 1000);
                var rgbKey = PBKDF2.GetBytes(32);

                Trace.WriteLine("IV " + Convert.ToBase64String(iv));
                Trace.WriteLine("SALT " + Convert.ToBase64String(saltBuffer));
                Trace.WriteLine("EXTRACT " + Convert.ToBase64String(extract));
                Trace.WriteLine("KEY " + Convert.ToBase64String(key));
                Trace.WriteLine("RGBKEY " + Convert.ToBase64String(rgbKey));

                ICryptoTransform decryptor = cypher.CreateDecryptor(rgbKey, iv);

                using (MemoryStream stream = new MemoryStream(extract))
                {
                    using (CryptoStream cryptoStream = new CryptoStream(stream, decryptor, CryptoStreamMode.Read))
                    {
                        cryptoStream.Read(output, 0, output.Length);
                        return Encoding.ASCII.GetString(output);
                    }
                }
            }

        }


    }
}