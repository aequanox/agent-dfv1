﻿using System.IO;

namespace Switrace
{
    class CommandsCodes
    {
        static public string COMMAND_SET;
        static public byte GET_DEVICETYPE;
        static public byte GET_PCBREVISION;
        static public byte GET_STATS;
        static public byte GET_HUMIDITIES;
        static public byte GET_COMPANY;
        static public byte GET_FWVERSION;
        static public byte SET_DEVICEID;
        static public byte GET_DEVICEID;
        static public byte GET_CAPACITY;
        static public byte GET_SETTINGS;
        static public byte GET_SETTINGS_2;
        static public byte GET_HSL;
        static public byte GET_LOCK_SETTINGS;
        static public byte GET_TEMPERATURES;
        static public byte GET_TEMPERATUREREADING;
        static public byte GET_TEMPERATURES2;
        static public byte GET_TEMPERATUREREADING2;
        static public byte ERASE_TEMPERATURE_MEMORY;
        static public byte GET_SYSTEM_STATUS;
        static public byte SET_CURRENTTIME;
        static public byte WRITE_SETTINGS;
        static public byte WRITE_SETTINGS_2;
        static public byte GET_SENSORTYPE;
        static public byte WRITE_LOCK_SETTINGS;
        static public byte GET_BATTERY_PERCENTAGE;
        static public byte GET_BATTERY_VALUE;
        static public byte GET_TIMESTAMP;

        static public void Codes()
        {
            CodesStandard();
        }

        static public void CodesStandard()
        {
            COMMAND_SET = "STANDARD";
            GET_DEVICETYPE = 0x01;
            GET_PCBREVISION = 0x02;
            GET_COMPANY = 0x10;
            GET_FWVERSION = 0x20;
            SET_DEVICEID = 0x30;
            GET_DEVICEID = 0x31;
            GET_CAPACITY = 0x35;
            GET_SETTINGS = 0x37;
            GET_LOCK_SETTINGS = 0x38;
            GET_TEMPERATURES = 0x40;
            GET_TEMPERATUREREADING = 0x41;
            GET_HUMIDITIES = 0x42;
            GET_TEMPERATURES2 = 0x43;
            GET_TEMPERATUREREADING2 = 0x44;
            GET_STATS = 0x45;
            GET_SETTINGS_2 = 0x46;
            GET_HSL = 0x47;
            WRITE_LOCK_SETTINGS = 0x73;
            ERASE_TEMPERATURE_MEMORY = 0x80;
            GET_SYSTEM_STATUS = 0x81;
            SET_CURRENTTIME = 0x82;
            WRITE_SETTINGS = 0x83;
            GET_SENSORTYPE = 0x84;
            GET_BATTERY_PERCENTAGE = 0x85;
            GET_BATTERY_VALUE = 0x86;
            WRITE_SETTINGS_2 = 0x87;
            GET_TIMESTAMP = 0x99;
        }

        static public void CodesDataflow()
        {
            COMMAND_SET = "DATAFLOW";
            GET_DEVICETYPE = 0xC1;
            GET_PCBREVISION = 0x02;
            GET_COMPANY = 0x10;
            GET_FWVERSION = 0xD0;
            SET_DEVICEID = 0xD1;
            GET_DEVICEID = 0xD2;
            GET_CAPACITY = 0x35;
            GET_SETTINGS = 0xD3;
            GET_TEMPERATURES = 0xD4;
            GET_TEMPERATUREREADING = 0xD5;
            GET_HUMIDITIES = 0x42;
            GET_TEMPERATURES2 = 0x43;
            GET_TEMPERATUREREADING2 = 0x44;
            GET_STATS = 0x45;
            GET_SETTINGS_2 = 0x46;
            GET_HSL = 0x47;
            ERASE_TEMPERATURE_MEMORY = 0xD8;
            GET_SYSTEM_STATUS = 0xD9;
            SET_CURRENTTIME = 0xDA;
            WRITE_SETTINGS = 0xDB;
            WRITE_LOCK_SETTINGS = 0x73;
            GET_SENSORTYPE = 0xDC;
            GET_BATTERY_PERCENTAGE = 0x85;
            GET_BATTERY_VALUE = 0x86;
            WRITE_SETTINGS_2 = 0x87;
            GET_TIMESTAMP = 0x99;
        }
    }
}
