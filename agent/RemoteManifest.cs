﻿using System;
using System.Xml.Linq;
using Microsoft.Win32;
using System.Net;

namespace Switrace
{
    class Manifest
    {
        public Version version;
        public Uri updateUri;

        public Manifest()
        {
            version = null;
            updateUri = null;
        }
    }

    class RemoteManifest
    {
        static public Manifest get(Uri uri)
        {
            Manifest manifest = new Manifest();
            try
            {
                string location = uri.ToString();
                foreach (var el in XElement.Load(location).Elements())
                {
                    switch (el.Name.ToString())
                    {
                        case "version":
                            manifest.version = new Version(el.Value);
                            break;
                        case "url":
                            manifest.updateUri = new Uri(el.Value);
                            break;
                        default:
                            break;
                    }
                }

            }
            catch (WebException ex)
            {
                Console.Write("{0} -> ", ex.TargetSite);
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.Write("{0} -> ", ex.TargetSite);
                Console.WriteLine(ex.Message);
            }

            return manifest;
        }


        // Retrieve update manifest url from registry
        static public void ReadFromRegistry()
        {
            try
            {
                using (RegistryKey key = Registry.CurrentUser.OpenSubKey("Software\\Switrace\\Dataflow\\Agent"))
                {
                    if (key != null)
                    {
#if DEBUG
                        Object uri1 = key.GetValue("ManifestUrl_debug");
                        Object uri2 = key.GetValue("ManifestUrl_debug");
#else
                        Object uri1 = key.GetValue("ManifestUrl_1");
                        Object uri2 = key.GetValue("ManifestUrl_2");
#endif
                        AutoUpdateConfig.manifestUris.Add(new Uri(uri1.ToString()));
                        AutoUpdateConfig.manifestUris.Add(new Uri(uri2.ToString()));
                    }
                }
            }
            catch (Exception ex)  //just for demonstration...it's always best to handle specific exceptions
            {
                //react appropriately
                Console.WriteLine(ex.Message);
            }
        }

        //    public void parse()
        //    {
        //        try
        //        {
        //            reader = new XmlTextReader(murl);
        //            reader.MoveToContent();
        //            string elementName = "";
        //            if ((reader.NodeType == XmlNodeType.Element) && (reader.Name == AutoUpdateConfig.manifestRoot))
        //            {
        //                while (reader.Read())
        //                {
        //                    if ((reader.NodeType == XmlNodeType.Element))
        //                    {
        //                        elementName = reader.Name;
        //                    }
        //                    else
        //                    {
        //                        if ((reader.NodeType == XmlNodeType.Text) && (reader.HasValue))
        //                        {
        //                            switch (elementName)
        //                            {
        //                                case "version":
        //                                    manifest.newVersion = new Version(reader.Value);
        //                                    break;
        //                                case "url":
        //                                    manifest.downloadUrl = reader.Value;
        //                                    break;
        //                            }
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //        catch (WebException ex)
        //        {
        //            manifestError = true;
        //            //MessageBox.Show("Manifest:" + ex.Message);
        //        }
        //        catch (Exception ex)
        //        {
        //            manifestError = true;
        //            //MessageBox.Show("Manifest:" + ex.Message);
        //        }
        //        finally
        //        {
        //            if (reader != null)
        //                reader.Close();
        //        }
        //    }

        //    var manifest = new { newVersion = "", downloadUrl = "" };
        //    bool manifestError = false;
        //    Version newVersion = null;
        //    string downloadUrl = "";
        //    XmlTextReader reader = null;

        //        foreach (var murl in AutoUpdateConfig.manifestUrl)
        //        {


        //        }

        //        return manifest;
    }
}
