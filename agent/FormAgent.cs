﻿
//NOTE: In order for this program to "find" a USB device with a given VID and PID, 
//both the VID and PID in the USB device descriptor (in the USB firmware on the 
//microcontroller), as well as in this PC application source code, must match.
//To change the VID/PID in this PC application source code, scroll down to the 
//CheckIfPresentAndGetUSBDevicePath() function, and change the line that currently
//reads:

//   String DeviceIDToFind = "Vid_04d8&Pid_003f";


//NOTE 2: This C# program makes use of several functions in setupapi.dll and
//other Win32 DLLs.  However, one cannot call the functions directly in a 
//32-bit DLL if the project is built in "Any CPU" mode, when run on a 64-bit OS.
//When configured to build an "Any CPU" executable, the executable will "become"
//a 64-bit executable when run on a 64-bit OS.  On a 32-bit OS, it will run as 
//a 32-bit executable, and the pointer sizes and other aspects of this 
//application will be compatible with calling 32-bit DLLs.

//Therefore, on a 64-bit OS, this application will not work unless it is built in
//"x86" mode.  When built in this mode, the exectuable always runs in 32-bit mode
//even on a 64-bit OS.  This allows this application to make 32-bit DLL function 
//calls, when run on either a 32-bit or 64-bit OS.

//By default, on a new project, C# normally wants to build in "Any CPU" mode.  
//To switch to "x86" mode, open the "Configuration Manager" window.  In the 
//"Active solution platform:" drop down box, select "x86".  If this option does
//not appear, select: "<New...>" and then select the x86 option in the 
//"Type or select the new platform:" drop down box.  

using System;
using System.ComponentModel;

using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Microsoft.Win32.SafeHandles;
using System.Runtime.InteropServices;
using System.Threading;
using System.Diagnostics;
using System.IO;
using System.Xml;

using System.Net;
using Switrace.DllWrappers;
using System.Threading.Tasks;

namespace Switrace
{
    public partial class FormAgent : Form
    {
        //--------------- Global Varibles Section ------------------
        //USB related variables that need to have wide scope.

        bool AttachedState = false;						//Need to keep track of the USB device attachment status for proper plug and play operation.
        bool AttachedButBroken = false;
        bool ReadSettings = true;
        bool refreshForm, previouslyAttached, updateChecked;

        SafeFileHandle WriteHandleToUSBDevice = null;
        SafeFileHandle ReadHandleToUSBDevice = null;
        String DevicePath = null;   //Need the find the proper device path before you can open file handles.

        DeviceSystemStates SystemStatus;		//Updated by ReadWriteThread, read by FormUpdateTimer tick handler (needs to be atomic)

        byte[] firstRow = null;
        byte[] secondRow = null;

        //Globally Unique Identifier (GUID) for HID class devices.  Windows uses GUIDs to identify things.
        //Guid InterfaceClassGuid = new Guid(0x4d1e55b2, 0xf16f, 0x11cf, 0x88, 0xcb, 0x00, 0x11, 0x11, 0x00, 0x00, 0x30);
        Guid InterfaceClassGuid = new Guid("4d1e55b2-f16f-11cf-88cb-001111000030");
        //--------------- End of Global Varibles ------------------

        Device DataLogger;
        WSServer wsServer;
        Manifest manifest = new Manifest();
        EventHandler updateHandler;

        //Need to check "Allow unsafe code" checkbox in build properties to use unsafe keyword.  Unsafe is needed to
        //properly interact with the unmanged C++ style APIs used to find and connect with the USB device.
        public unsafe FormAgent()
        {
            //Additional constructor code

            //Initialize tool tips, to provide pop up help when the mouse cursor is moved over objects on the form.
            //ToggleLEDToolTip.SetToolTip(this.ToggleLEDs_btn, "Sends a packet of data to the USB device.");
            //PushbuttonStateTooltip.SetToolTip(this.PushbuttonState_lbl, "Try pressing pushbuttons on the USB demo board/PIM.");

            //Register for WM_DEVICECHANGE notifications.  This code uses these messages to detect plug and play connection/disconnection events for USB devices
            Dll.DEV_BROADCAST_DEVICEINTERFACE DeviceBroadcastHeader = new Dll.DEV_BROADCAST_DEVICEINTERFACE();
            DeviceBroadcastHeader.dbcc_devicetype = Dll.DBT_DEVTYP_DEVICEINTERFACE;
            DeviceBroadcastHeader.dbcc_size = (uint)Marshal.SizeOf(DeviceBroadcastHeader);
            DeviceBroadcastHeader.dbcc_reserved = 0;	//Reserved says not to use...
            DeviceBroadcastHeader.dbcc_classguid = InterfaceClassGuid;

            //Need to get the address of the DeviceBroadcastHeader to call RegisterDeviceNotification(), but
            //can't use "&DeviceBroadcastHeader".  Instead, using a roundabout means to get the address by 
            //making a duplicate copy using Marshal.StructureToPtr().
            IntPtr pDeviceBroadcastHeader = IntPtr.Zero;  //Make a pointer.
            pDeviceBroadcastHeader = Marshal.AllocHGlobal(Marshal.SizeOf(DeviceBroadcastHeader)); //allocate memory for a new DEV_BROADCAST_DEVICEINTERFACE structure, and return the address 
            Marshal.StructureToPtr(DeviceBroadcastHeader, pDeviceBroadcastHeader, false);  //Copies the DeviceBroadcastHeader structure into the memory already allocated at DeviceBroadcastHeaderWithPointer
            Dll.RegisterDeviceNotification(this.Handle, pDeviceBroadcastHeader, Dll.DEVICE_NOTIFY_WINDOW_HANDLE);


            firstRow = new byte[10];
            secondRow = new byte[10];

            this.Visible = false;
            this.ShowInTaskbar = false;

            InitializeComponent();

            wsServer = new WSServer();
            wsServer.openWebSocket();

            //// Check for updates after ten minutes from starting program
            //Task.Delay(TimeSpan.FromMinutes(10)).ContinueWith(t =>
            //{
            //    if (!updateChecked)
            //    {
            //        updateTask(null, EventArgs.Empty);
            //    }
            //});

            //// Check for updates every XX hours
            //double ms = TimeSpan.FromHours(10).TotalMilliseconds;
            //System.Timers.Timer aTimer = new System.Timers.Timer(ms);
            //aTimer.Elapsed += new System.Timers.ElapsedEventHandler((s, ea) => {
            //    checkForUpdates(null, EventArgs.Empty);
            //});
            //aTimer.Enabled = true;

            //Now make an initial attempt to find the USB device, if it was already connected to the PC and enumerated prior to launching the application.
            //If it is connected and present, we should open read and write handles to the device so we can communicate with it later.
            //If it was not connected, we will have to wait until the user plugs the device in, and the WM_DEVICECHANGE callback function can process
            //the message and again search for the device.
            if (CheckIfPresentAndGetUSBDevicePath())	//Check and make sure at least one device with matching VID/PID is attached
            {
                uint ErrorStatusWrite;
                uint ErrorStatusRead;


                //We now have the proper device path, and we can finally open read and write handles to the device.
                WriteHandleToUSBDevice = Dll.CreateFile(DevicePath, Dll.GENERIC_WRITE, Dll.FILE_SHARE_READ | Dll.FILE_SHARE_WRITE, IntPtr.Zero, Dll.OPEN_EXISTING, 0, IntPtr.Zero);
                ErrorStatusWrite = (uint)Marshal.GetLastWin32Error();
                ReadHandleToUSBDevice = Dll.CreateFile(DevicePath, Dll.GENERIC_READ, Dll.FILE_SHARE_READ | Dll.FILE_SHARE_WRITE, IntPtr.Zero, Dll.OPEN_EXISTING, 0, IntPtr.Zero);
                ErrorStatusRead = (uint)Marshal.GetLastWin32Error();

                 if ((ErrorStatusWrite == Dll.ERROR_SUCCESS) && (ErrorStatusRead == Dll.ERROR_SUCCESS))
                {
                    AttachedState = true;		//Let the rest of the PC application know the USB device is connected, and it is safe to read/write to it
                    AttachedButBroken = false;
                    StatusBox_lbl.Text = "attached";
                }
                else //for some reason the device was physically plugged in, but one or both of the read/write handles didn't open successfully...
                {
                    AttachedState = false;		//Let the rest of this application known not to read/write to the device.
                    AttachedButBroken = true;	//Flag so that next time a WM_DEVICECHANGE message occurs, can retry to re-open read/write pipes
                    if (ErrorStatusWrite == Dll.ERROR_SUCCESS)
                        WriteHandleToUSBDevice.Close();
                    if (ErrorStatusRead == Dll.ERROR_SUCCESS)
                        ReadHandleToUSBDevice.Close();
                }
            }
            else	//Device must not be connected (or not programmed with correct firmware)
            {
                AttachedState = false;
                AttachedButBroken = false;
            }

            //Recommend performing USB read/write operations in a separate thread.  Otherwise,
            //the Read/Write operations are effectively blocking functions and can lock up the
            //user interface if the I/O operations take a long time to complete.
            ReadWriteThread.RunWorkerAsync();
            version_lbl.Text = "Version " + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;
        }


        //FUNCTION:	CheckIfPresentAndGetUSBDevicePath()
        //PURPOSE:	Check if a USB device is currently plugged in with a matching VID and PID
        //INPUT:	Uses globally declared String DevicePath, globally declared GUID, and the MY_DEVICE_ID constant.
        //OUTPUT:	Returns BOOL.  TRUE when device with matching VID/PID found.  FALSE if device with VID/PID could not be found.
        //			When returns TRUE, the globally accessable "DetailedInterfaceDataStructure" will contain the device path
        //			to the USB device with the matching VID/PID.

        /* 
        Before we can "connect" our application to our USB embedded device, we must first find the device.
        A USB bus can have many devices simultaneously connected, so somehow we have to find our device only.
        This is done with the Vendor ID (VID) and Product ID (PID).  Each USB product line should have
        a unique combination of VID and PID.  

        Microsoft has created a number of functions which are useful for finding plug and play devices.  Documentation
        for each function used can be found in the MSDN library.  We will be using the following functions (unmanaged C functions):

        SetupDiGetClassDevs()					//provided by setupapi.dll, which comes with Windows
        SetupDiEnumDeviceInterfaces()			//provided by setupapi.dll, which comes with Windows
        GetLastError()							//provided by kernel32.dll, which comes with Windows
        SetupDiDestroyDeviceInfoList()			//provided by setupapi.dll, which comes with Windows
        SetupDiGetDeviceInterfaceDetail()		//provided by setupapi.dll, which comes with Windows
        SetupDiGetDeviceRegistryProperty()		//provided by setupapi.dll, which comes with Windows
        CreateFile()							//provided by kernel32.dll, which comes with Windows

        In order to call these unmanaged functions, the Marshal class is very useful.
             
        We will also be using the following unusual data types and structures.  Documentation can also be found in
        the MSDN library:

        PSP_DEVICE_INTERFACE_DATA
        PSP_DEVICE_INTERFACE_DETAIL_DATA
        SP_DEVINFO_DATA
        HDEVINFO
        HANDLE
        GUID

        The ultimate objective of the following code is to get the device path, which will be used elsewhere for getting
        read and write handles to the USB device.  Once the read/write handles are opened, only then can this
        PC application begin reading/writing to the USB device using the WriteFile() and ReadFile() functions.

        Getting the device path is a multi-step round about process, which requires calling several of the
        SetupDixxx() functions provided by setupapi.dll.
        */

        bool CheckIfPresentAndGetUSBDevicePath()
        {

            try
            {
                IntPtr DeviceInfoTable = IntPtr.Zero;
                Dll.SP_DEVICE_INTERFACE_DATA InterfaceDataStructure = new Dll.SP_DEVICE_INTERFACE_DATA();
                Dll.SP_DEVICE_INTERFACE_DETAIL_DATA DetailedInterfaceDataStructure = new Dll.SP_DEVICE_INTERFACE_DETAIL_DATA();
                Dll.SP_DEVINFO_DATA DevInfoData = new Dll.SP_DEVINFO_DATA();

                uint InterfaceIndex = 0;
                uint dwRegType = 0;
                uint dwRegSize = 0;
                uint dwRegSize2 = 0;
                uint StructureSize = 0;
                IntPtr PropertyValueBuffer = IntPtr.Zero;
                bool MatchFound = false;
                uint ErrorStatus;
                uint LoopCounter = 0;

                //Use the formatting: "Vid_xxxx&Pid_xxxx" where xxxx is a 16-bit hexadecimal number.
                //Make sure the value appearing in the parathesis matches the USB device descriptor
                //of the device that this aplication is intending to find.
                String DeviceIDToFind = "Vid_04D8&Pid_0054";

                //First populate a list of plugged in devices (by specifying "DIGCF_PRESENT"), which are of the specified class GUID. 
                DeviceInfoTable = Dll.SetupDiGetClassDevs(ref InterfaceClassGuid, IntPtr.Zero, IntPtr.Zero, Dll.DIGCF_PRESENT | Dll.DIGCF_DEVICEINTERFACE);

                if (DeviceInfoTable != IntPtr.Zero)
                {
                    //Now look through the list we just populated.  Vid_04d8&Pid_0054We are trying to see if any of them match our device. 
                    while (true)
                    {
                        InterfaceDataStructure.cbSize = (uint)Marshal.SizeOf(InterfaceDataStructure);
                        if (Dll.SetupDiEnumDeviceInterfaces(DeviceInfoTable, IntPtr.Zero, ref InterfaceClassGuid, InterfaceIndex, ref InterfaceDataStructure))
                        {
                            ErrorStatus = (uint)Marshal.GetLastWin32Error();
                            if (ErrorStatus == Dll.ERROR_NO_MORE_ITEMS)	//Did we reach the end of the list of matching devices in the DeviceInfoTable?
                            {	//Cound not find the device.  Must not have been attached.
                                Dll.SetupDiDestroyDeviceInfoList(DeviceInfoTable);	//Clean up the old structure we no longer need.
                                return false;
                            }
                        }
                        else	//Else some other kind of unknown error ocurred...
                        {
                            ErrorStatus = (uint)Marshal.GetLastWin32Error();
                            Dll.SetupDiDestroyDeviceInfoList(DeviceInfoTable);	//Clean up the old structure we no longer need.
                            return false;
                        }

                        //Now retrieve the hardware ID from the registry.  The hardware ID contains the VID and PID, which we will then 
                        //check to see if it is the correct device or not.

                        //Initialize an appropriate SP_DEVINFO_DATA structure.  We need this structure for SetupDiGetDeviceRegistryProperty().
                        DevInfoData.cbSize = (uint)Marshal.SizeOf(DevInfoData);
                        Dll.SetupDiEnumDeviceInfo(DeviceInfoTable, InterfaceIndex, ref DevInfoData);

                        //First query for the size of the hardware ID, so we can know how big a buffer to allocate for the data.
                        Dll.SetupDiGetDeviceRegistryProperty(DeviceInfoTable, ref DevInfoData, Dll.SPDRP_HARDWAREID, ref dwRegType, IntPtr.Zero, 0, ref dwRegSize);

                        //Allocate a buffer for the hardware ID.
                        //Should normally work, but could throw exception "OutOfMemoryException" if not enough resources available.
                        PropertyValueBuffer = Marshal.AllocHGlobal((int)dwRegSize);

                        //Retrieve the hardware IDs for the current device we are looking at.  PropertyValueBuffer gets filled with a 
                        //REG_MULTI_SZ (array of null terminated strings).  To find a device, we only care about the very first string in the
                        //buffer, which will be the "device ID".  The device ID is a string which contains the VID and PID, in the example 
                        //format "Vid_04d8&Pid_003f".
                        Dll.SetupDiGetDeviceRegistryProperty(DeviceInfoTable, ref DevInfoData, Dll.SPDRP_HARDWAREID, ref dwRegType, PropertyValueBuffer, dwRegSize, ref dwRegSize2);

                        //Now check if the first string in the hardware ID matches the device ID of the USB device we are trying to find.
                        String DeviceIDFromRegistry = Marshal.PtrToStringUni(PropertyValueBuffer); //Make a new string, fill it with the contents from the PropertyValueBuffer

                        Marshal.FreeHGlobal(PropertyValueBuffer);		//No longer need the PropertyValueBuffer, free the memory to prevent potential memory leaks

                        //Convert both strings to lower case.  This makes the code more robust/portable accross OS Versions
                        DeviceIDFromRegistry = DeviceIDFromRegistry.ToLowerInvariant();
                        DeviceIDToFind = DeviceIDToFind.ToLowerInvariant();
                        //Now check if the hardware ID we are looking at contains the correct VID/PID
                        MatchFound = DeviceIDFromRegistry.Contains(DeviceIDToFind);
                        if (MatchFound == true)
                        {
                            //Device must have been found.  In order to open I/O file handle(s), we will need the actual device path first.
                            //We can get the path by calling SetupDiGetDeviceInterfaceDetail(), however, we have to call this function twice:  The first
                            //time to get the size of the required structure/buffer to hold the detailed interface data, then a second time to actually 
                            //get the structure (after we have allocated enough memory for the structure.)
                            DetailedInterfaceDataStructure.cbSize = (uint)Marshal.SizeOf(DetailedInterfaceDataStructure);
                            //First call populates "StructureSize" with the correct value
                            Dll.SetupDiGetDeviceInterfaceDetail(DeviceInfoTable, ref InterfaceDataStructure, IntPtr.Zero, 0, ref StructureSize, IntPtr.Zero);
                            //Need to call SetupDiGetDeviceInterfaceDetail() again, this time specifying a pointer to a SP_DEVICE_INTERFACE_DETAIL_DATA buffer with the correct size of RAM allocated.
                            //First need to allocate the unmanaged buffer and get a pointer to it.
                            IntPtr pUnmanagedDetailedInterfaceDataStructure = IntPtr.Zero;  //Declare a pointer.
                            pUnmanagedDetailedInterfaceDataStructure = Marshal.AllocHGlobal((int)StructureSize);    //Reserve some unmanaged memory for the structure.
                            DetailedInterfaceDataStructure.cbSize = 6; //Initialize the cbSize parameter (4 bytes for DWORD + 2 bytes for unicode null terminator)
                            Marshal.StructureToPtr(DetailedInterfaceDataStructure, pUnmanagedDetailedInterfaceDataStructure, false); //Copy managed structure contents into the unmanaged memory buffer.

                            //Now call SetupDiGetDeviceInterfaceDetail() a second time to receive the device path in the structure at pUnmanagedDetailedInterfaceDataStructure.
                            if (Dll.SetupDiGetDeviceInterfaceDetail(DeviceInfoTable, ref InterfaceDataStructure, pUnmanagedDetailedInterfaceDataStructure, StructureSize, IntPtr.Zero, IntPtr.Zero))
                            {
                                //Need to extract the path information from the unmanaged "structure".  The path starts at (pUnmanagedDetailedInterfaceDataStructure + sizeof(DWORD)).
                                IntPtr pToDevicePath = new IntPtr((uint)pUnmanagedDetailedInterfaceDataStructure.ToInt32() + 4);  //Add 4 to the pointer (to get the pointer to point to the path, instead of the DWORD cbSize parameter)
                                DevicePath = Marshal.PtrToStringUni(pToDevicePath); //Now copy the path information into the globally defined DevicePath String.

                                //We now have the proper device path, and we can finally use the path to open I/O handle(s) to the device.
                                Dll.SetupDiDestroyDeviceInfoList(DeviceInfoTable);	//Clean up the old structure we no longer need.
                                Marshal.FreeHGlobal(pUnmanagedDetailedInterfaceDataStructure);  //No longer need this unmanaged SP_DEVICE_INTERFACE_DETAIL_DATA buffer.  We already extracted the path information.
                                return true;    //Returning the device path in the global DevicePath String
                            }
                            else //Some unknown failure occurred
                            {
                                uint ErrorCode = (uint)Marshal.GetLastWin32Error();
                                Dll.SetupDiDestroyDeviceInfoList(DeviceInfoTable);	//Clean up the old structure.
                                Marshal.FreeHGlobal(pUnmanagedDetailedInterfaceDataStructure);  //No longer need this unmanaged SP_DEVICE_INTERFACE_DETAIL_DATA buffer.  We already extracted the path information.
                                return false;
                            }
                        }

                        InterfaceIndex++;
                        //Keep looping until we either find a device with matching VID and PID, or until we run out of devices to check.
                        //However, just in case some unexpected error occurs, keep track of the number of loops executed.
                        //If the number of loops exceeds a very large number, exit anyway, to prevent inadvertent infinite looping.
                        LoopCounter++;
                        if (LoopCounter == 10000000)	//Surely there aren't more than 10 million devices attached to any forseeable PC...
                        {
                            return false;
                        }
                    }//end of while(true)
                }
                return false;
            }//end of try
            catch
            {
                //Something went wrong if PC gets here.  Maybe a Marshal.AllocHGlobal() failed due to insufficient resources or something.
                return false;
            }
        }

        //This is a callback function that gets called when a Windows message is received by the form.
        //We will receive various different types of messages, but the ones we really want to use are the WM_DEVICECHANGE messages.
        protected override void WndProc(ref Message m)
        {
            if (m.Msg == Dll.WM_DEVICECHANGE)
            {
                if (((int)m.WParam == Dll.DBT_DEVICEARRIVAL) || ((int)m.WParam == Dll.DBT_DEVICEREMOVEPENDING) || ((int)m.WParam == Dll.DBT_DEVICEREMOVECOMPLETE) || ((int)m.WParam == Dll.DBT_CONFIGCHANGED))
                {
                    Console.WriteLine(m.WParam.ToString());
                    //WM_DEVICECHANGE messages by themselves are quite generic, and can be caused by a number of different
                    //sources, not just your USB hardware device.  Therefore, must check to find out if any changes relavant
                    //to your device (with known VID/PID) took place before doing any kind of opening or closing of handles/endpoints.
                    //(the message could have been totally unrelated to your application/USB device)

                    if (CheckIfPresentAndGetUSBDevicePath())	//Check and make sure at least one device with matching VID/PID is attached
                    {
                        //If executes to here, this means the device is currently attached and was found.
                        //This code needs to decide however what to do, based on whether or not the device was previously known to be
                        //attached or not.
                        if ((AttachedState == false) || (AttachedButBroken == true))	//Check the previous attachment state
                        {
                            uint ErrorStatusWrite;
                            uint ErrorStatusRead;

                            //We obtained the proper device path (from CheckIfPresentAndGetUSBDevicePath() function call), and it
                            //is now possible to open read and write handles to the device.
                            WriteHandleToUSBDevice = Dll.CreateFile(DevicePath, Dll.GENERIC_WRITE, Dll.FILE_SHARE_READ | Dll.FILE_SHARE_WRITE, IntPtr.Zero, Dll.OPEN_EXISTING, 0, IntPtr.Zero);
                            ErrorStatusWrite = (uint)Marshal.GetLastWin32Error();
                            ReadHandleToUSBDevice = Dll.CreateFile(DevicePath, Dll.GENERIC_READ, Dll.FILE_SHARE_READ | Dll.FILE_SHARE_WRITE, IntPtr.Zero, Dll.OPEN_EXISTING, 0, IntPtr.Zero);
                            ErrorStatusRead = (uint)Marshal.GetLastWin32Error();

                            if ((ErrorStatusWrite == Dll.ERROR_SUCCESS) && (ErrorStatusRead == Dll.ERROR_SUCCESS))
                            {
                                AttachedState = true;		//Let the rest of the PC application know the USB device is connected, and it is safe to read/write to it
                                AttachedButBroken = false;
                                StatusBox_lbl.Text = "attached";
                            }
                            else //for some reason the device was physically plugged in, but one or both of the read/write handles didn't open successfully...
                            {
                                AttachedState = false;		//Let the rest of this application known not to read/write to the device.
                                AttachedButBroken = true;	//Flag so that next time a WM_DEVICECHANGE message occurs, can retry to re-open read/write pipes
                                if (ErrorStatusWrite == Dll.ERROR_SUCCESS)
                                    WriteHandleToUSBDevice.Close();
                                if (ErrorStatusRead == Dll.ERROR_SUCCESS)
                                    ReadHandleToUSBDevice.Close();
                            }
                        }
                        //else we did find the device, but AttachedState was already true.  In this case, don't do anything to the read/write handles,
                        //since the WM_DEVICECHANGE message presumably wasn't caused by our USB device.  
                    }
                    else	//Device must not be connected (or not programmed with correct firmware)
                    {
                        if (AttachedState == true)		//If it is currently set to true, that means the device was just now disconnected
                        {
                            AttachedState = false;
                            WriteHandleToUSBDevice.Close();
                            ReadHandleToUSBDevice.Close();
                        }
                        AttachedState = false;
                        AttachedButBroken = false;
                    }
                }
            } //end of: if(m.Msg == WM_DEVICECHANGE)

            base.WndProc(ref m);
        } //end of: WndProc() function


        /*This thread does the actual USB read/write operations (but only when AttachedState == true) to the USB device.
        It is generally preferrable to write applications so that read and write operations are handled in a separate
        thread from the main form.  This makes it so that the main form can remain responsive, even if the I/O operations
        take a very long time to complete.

        Since this is a separate thread, this code below executes independently from the rest of the
        code in this application.  All this thread does is read and write to the USB device.  It does not update
        the form directly with the new information it obtains (such as the ANxx/POT Voltage or pushbutton state).
        The information that this thread obtains is stored in atomic global variables.
        Form updates are handled by the FormUpdateTimer Tick event handler function.

        This application sends packets to the endpoint buffer on the USB device by using the "WriteFile()" function.
        This application receives packets from the endpoint buffer on the USB device by using the "ReadFile()" function.
        Both of these functions are documented in the MSDN library.  Calling ReadFile() is a not perfectly straight
        foward in C# environment, since one of the input parameters is a pointer to a buffer that gets filled by ReadFile().
        The ReadFile() function is therefore called through a wrapper function ReadFileManagedBuffer().

        All ReadFile() and WriteFile() operations in this example project are synchronous.  They are blocking function
        calls and only return when they are complete, or if they fail because of some event, such as the user unplugging
        the device.  It is possible to call these functions with "overlapped" structures, and use them as non-blocking
        asynchronous I/O function calls.  

        Note:  This code may perform differently on some machines when the USB device is plugged into the host through a
        USB 2.0 hub, as opposed to a direct connection to a root port on the PC.  In some cases the data rate may be slower
        when the device is connected through a USB 2.0 hub.  This performance difference is believed to be caused by
        the issue described in Microsoft knowledge base article 940021:
        http://support.microsoft.com/kb/940021/en-us 

        Higher effective bandwidth (up to the maximum offered by interrupt endpoints), both when connected
        directly and through a USB 2.0 hub, can generally be achieved by queuing up multiple pending read and/or
        write requests simultaneously.  This can be done when using	asynchronous I/O operations (calling ReadFile() and
        WriteFile()	with overlapped structures).  The Microchip	HID USB Bootloader application uses asynchronous I/O
        for some USB operations and the source code can be used	as an example.*/

        private void ReadWriteThread_DoWork(object sender, DoWorkEventArgs e)
        {
            FirmwareCommands Firmware;

            while (true)
            {
                try
                {
                    if (AttachedState == true)	//Do not try to use the read/write handles unless the USB device is attached and ready
                    {
                        if (ReadSettings == true)
                        {
                            ReadSettings = false;

                            statusLabel.Invoke((Action)delegate { statusLabel.Wait("Loading data...Please wait"); });

                            Firmware = new FirmwareCommands(WriteHandleToUSBDevice, ReadHandleToUSBDevice);
                            DataLogger = new Device(Firmware);

                            statusLabel.Invoke((Action)delegate { statusLabel.Default(); });

                            if (DataLogger.DeviceId == null)
                            {
                                Firmware.SETDeviceId();
                                ReadSettings = true;
                            }
                            refreshForm = true;
                        }

                        SystemStatus = DataLogger.SystemStatus;

                    } //end of: if(AttachedState == true)
                    else
                    {
                        ReadSettings = true;
                        //Add a small delay.  Otherwise, this while(true) loop can execute very fast and cause 
                        //high CPU utilization, with no particular benefit to the application.
                        Thread.Sleep(50);
                    }
                }
                catch (NullReferenceException exc)
                {
                    if (exc.Message == "DEVICE_UNKNOWN")
                    {
                        switch (CommandsCodes.COMMAND_SET)
                        {
                            case "STANDARD":
                                CommandsCodes.CodesDataflow();
                                ReadSettings = AttachedState = true;
                                break;
                            case "DATAFLOW":
                                CommandsCodes.CodesStandard();
                                ReadSettings = AttachedState = true;
                                break;
                            default:
                                AttachedState = false;
                                break;
                        }
                    }
                }
                catch (Exception exc)
                {
                    Console.WriteLine(exc.Message);
                    //Exceptions can occur during the read or write operations.  For example,
                    //exceptions may occur if for instance the USB device is physically unplugged
                    //from the host while the above read/write functions are executing.

                    //Don't need to do anything special in this case.  The application will automatically
                    //re-establish communications based on the global AttachedState boolean variable used
                    //in conjunction with the WM_DEVICECHANGE messages to dyanmically respond to Plug and Play
                    //USB connection events.
                }

            } //end of while(true) loop
        }

        




        private void FormUpdateTimer_Tick(object sender, EventArgs e)
        {
            //This timer tick event handler function is used to update the user interface on the form, based on data
            //obtained asynchronously by the ReadWriteThread and the WM_DEVICECHANGE event handler functions.

            if (wsServer.ServerStarted)
            {
                serverStatus_txtbx.Text = "Server started";
                serverStatus_txtbx.BackColor = Color.LightGreen;
            }
            else
            {
                serverStatus_txtbx.Text = "Server not started";
                serverStatus_txtbx.BackColor = Color.Khaki;
            }

            if (AttachedState == true && AttachedButBroken == false)
            {
                //Device is connected and ready to communicate, enable user interface on the form 
                //StatusBox_lbl.Text = "Device Attached";
                if (DataLogger != null)
                {
                    SystemStatus_lbl.Text = SystemStatus.ToString();

                    if (refreshForm == true)
                    {
                        refreshForm = false;
                        try
                        {
                            previouslyAttached = true;
                            wsServer.loadDatalogger(DataLogger);
                            wsServer.SendMessage(new WSMessageToClient("deviceAttached"));

                            StatusBox_lbl.Text = DataLogger.GetDeviceDescription();

                            serial_txtbx.Text = DataLogger.SerialNumber.ToString();
                            //deviceId_lbl.Text = "Device ID : " + DataLogger.DeviceId();

                            FirstRow_txtbx.Text = DataLogger.CommentFirstRow;
                            SecondRow_txtbx.Text = DataLogger.CommentSecondRow;

                            StartDelay_txtbx.Text = DataLogger.StartDelay.ToString();

                            if (SystemStatus > DeviceSystemStates.BLANK)
                            {
                                if (DataLogger.AlarmLL.IsEnabled)
                                {
                                    chart_ll.Text = (DataLogger.TempMU == TempMU.FAHRENHEIT) ? degCtoF((int)DataLogger.AlarmLL.SetPoint).ToString() : DataLogger.AlarmLL.SetPoint.ToString();
                                }
                                if (DataLogger.AlarmL.IsEnabled)
                                {
                                    chart_l.Text = (DataLogger.TempMU == TempMU.FAHRENHEIT) ? degCtoF((int)DataLogger.AlarmL.SetPoint).ToString() : DataLogger.AlarmL.SetPoint.ToString();
                                }
                                if (DataLogger.AlarmH.IsEnabled)
                                {
                                    chart_h.Text = (DataLogger.TempMU == TempMU.FAHRENHEIT) ? degCtoF((int)DataLogger.AlarmH.SetPoint).ToString() : DataLogger.AlarmH.SetPoint.ToString();
                                }
                                if (DataLogger.AlarmHH.IsEnabled)
                                {
                                    chart_hh.Text = (DataLogger.TempMU == TempMU.FAHRENHEIT) ? degCtoF((int)DataLogger.AlarmHH.SetPoint).ToString() : DataLogger.AlarmHH.SetPoint.ToString();
                                }
                            }

                            measures_txtbx.Text = DataLogger.MeasuresNumber.ToString();
                            sampletime_txtbx.Text = DataLogger.SamplingTime.ToString();

                            if (DataLogger.TempMU == TempMU.FAHRENHEIT)
                            {
                                tempUnit_txtbx.Text = "Fahrenheit";
                            }
                            else
                            {
                                tempUnit_txtbx.Text = "Celsius";
                            }

                            SystemStatus_lbl.ForeColor = Color.Black;

                            if (SystemStatus == DeviceSystemStates.BLANK)
                            {
                                statusLabel.Write("I-PLUG needs to be configured.");
                            }

                            if (SystemStatus == DeviceSystemStates.READY)
                            {
                                SystemStatus_lbl.ForeColor = Color.LimeGreen;
                                statusLabel.Write("I-PLUG is ready to be used. Unplug the device and start it.");
                            }

                            if (SystemStatus == DeviceSystemStates.STOPPED)
                            {
                                //GetTemp_btn.Enabled = true;
                                //chart_btn.Enabled = true;
                                SystemStatus_lbl.ForeColor = Color.Red;
                                statusLabel.Write("Upload and/or Read the temperatures recorded in this I-PLUG.");
                            }

                            groupSettings.Enabled = true;
                            groupComments.Enabled = true;
                            groupAlarms.Enabled = true;
                        }
                        catch
                        {
                            // Caso in cui i valori siano fuori limite
                        }

                        Thread.Sleep(50);
                    }
                }

            }

            if ((AttachedState == false) || (AttachedButBroken == true))
            {
                if (previouslyAttached)
                {
                    wsServer.loadDatalogger(null);
                    wsServer.SendMessage(new WSMessageToClient("deviceDetached"));
                    previouslyAttached = false;
                }

                refreshForm = false;

                groupSettings.Enabled = false;
                groupComments.Enabled = false;
                groupAlarms.Enabled = false;

                //Device not available to communicate. Disable user interface on the form.
                StatusBox_lbl.Text = "not detected";

                SystemStatus_lbl.ForeColor = Color.Black;
                SystemStatus_lbl.Text = "Unknown";

                //deviceId_lbl.Text = "Device ID : Unknown";

                serial_txtbx.Text = "";

                FirstRow_txtbx.Text = "";
                SecondRow_txtbx.Text = "";

                StartDelay_txtbx.Text = "";

                measures_txtbx.Text = "";
                sampletime_txtbx.Text = "";

                statusLabel.Write("Please connect your I-PLUG");

                chart_ll.Text = "----";
                chart_l.Text = "----";
                chart_h.Text = "----";
                chart_hh.Text = "----";
            }
        }


        private decimal degCtoF(int tempC)
        {
            return Math.Round((decimal)((1.8 * tempC) + 32), 0, MidpointRounding.AwayFromZero);
        }

        private void send_labels_Click(object sender, EventArgs e)
        {
            firstRow = Encoding.ASCII.GetBytes((FirstRow_txtbx.Text + "               ").ToCharArray());
            secondRow = Encoding.ASCII.GetBytes((SecondRow_txtbx.Text + "               ").ToCharArray());
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.ShowInTaskbar = true;
            this.Visible = true;
            myNotifyIcon.Visible = false;
            this.WindowState = FormWindowState.Minimized;

        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                myNotifyIcon.Visible = true;
                this.Hide();
                e.Cancel = true;
            }
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            if (FormWindowState.Minimized == this.WindowState)
            {
                this.ShowInTaskbar = false;
                this.Visible = false;
                myNotifyIcon.Visible = true;
            }
            else if (FormWindowState.Normal == this.WindowState)
            {
                //myNotifyIcon.Visible = false;
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void support_txtbx_TextChanged(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void groupSettings_Enter(object sender, EventArgs e)
        {

        }

        private void serverResponse_rtb_LinkClicked(object sender, LinkClickedEventArgs e)
        {
            Process.Start(e.LinkText);
        }

        private void chart_btn_Click(object sender, EventArgs e)
        {
            //measuresForm = new Measures(DataLogger);
            //measuresForm.Show();
        }

        private void tempUnit_lbl_Click(object sender, EventArgs e)
        {

        }

        private void myNotifyIcon_DoubleClick(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Normal;
            this.ShowInTaskbar = true;
            //myNotifyIcon.Visible = false;
            this.Show();
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult ext;
            ext = MessageBox.Show("Do you really want to quit Dataflow Agent?", "Dataflow Agent", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (ext == DialogResult.Yes)
            {
                Application.Exit();
            }
        }


        private void updateTask(object sender, EventArgs e)
        {
            //Task.Run(() => checkForUpdates(sender, e));
        }


        //public void checkForUpdates(object sender, EventArgs e)
        //{
        //    Console.WriteLine("Checking for updates...");
        //    updateChecked = true;
        //    try
        //    {
        //        foreach (var uri in AutoUpdateConfig.manifestUris)
        //        {
        //            manifest = RemoteManifest.get(uri);
        //            if (manifest.version != null && manifest.updateUri != null)
        //                break;
        //        }

        //        if (manifest.version == null && manifest.updateUri == null)
        //            throw new Exception("Error retrieving update informations.");

        //        Version appVersion = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;
        //        if (appVersion.CompareTo(manifest.version) < 0)
        //        {
        //            Console.WriteLine("Update available.");

        //            if (sender == null)
        //            {
        //                // Initialize automatic update event handler
        //                updateHandler = (s, ea) => updateApplication(manifest.updateUri);
        //                myNotifyIcon.BalloonTipClicked -= updateHandler;
        //                myNotifyIcon.BalloonTipClicked += updateHandler;
        //                myNotifyIcon.ShowBalloonTip(5000, "Dataflow Agent", "An update is available for Dataflow Agent. Click here to install it.", ToolTipIcon.Info);
        //            }
        //            else
        //            {
        //                DialogResult res = MessageBox.Show("An update is available for Dataflow Agent. Would you like to install it?", "Dataflow Agent", MessageBoxButtons.YesNo);
        //                if (res == DialogResult.Yes)
        //                {
        //                    updateApplication(manifest.updateUri);
        //                }
        //            }
        //        }
        //        else
        //        {
        //            Console.WriteLine("Dataflow Agent is already up to date.");
        //            // Display the message only on user click
        //            if (sender != null)
        //            {
        //                MessageBox.Show("Dataflow Agent is already up to date.", "Dataflow Agent");
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message, "Dataflow Agent");
        //    }
        //}


        private void updateApplication(Uri updateUri)
        {
            FileInfo file = new FileInfo(AutoUpdateConfig.tempFile);
            try
            {
                using (WebClient Client = new WebClient())
                {
                    Client.DownloadFile(updateUri, file.FullName);
                    Process.Start(file.FullName);
                }
            }
            catch (WebException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }



        private void connectionPort_txtbx_KeyPress(object sender, KeyPressEventArgs evtargs)
        {
            evtargs.Handled = (!char.IsDigit(evtargs.KeyChar)) && (!char.IsControl(evtargs.KeyChar));
        }


} //public partial class FormManager : Form
} //namespace Datalogger