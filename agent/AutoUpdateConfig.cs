﻿using System;
using System.Collections.Generic;
using System.IO;


namespace Switrace
{
    class AutoUpdateConfig
    {
        static public List<Uri> manifestUris = new List<Uri>();
        static public string manifestRoot = "dfagent";
        static public string tempFile = Path.GetTempPath() + "dfagent_upd.msi";
    }
}
