﻿using System;
using System.Threading;
using System.Windows.Forms;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using Microsoft.Win32;
using System.Collections.Generic;

namespace Switrace
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>

        [STAThread]
        static void Main()
        {
            if (!MutualExclusion.Acquire("agent"))
            {
                return;
            }

            CommandsCodes.CodesDataflow();
            RemoteManifest.ReadFromRegistry();

            CertificateManager();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            AppDomain.CurrentDomain.AssemblyResolve += new ResolveEventHandler(CurrentDomain_AssemblyResolve);

            Application.Run(new FormAgent());
        }

        // Load embedded DLL automatically
        static Assembly CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args)
        {
            AssemblyName embeddedAssembly = new AssemblyName(args.Name);
            String resourceName = "Switrace.EmbeddedResources." + embeddedAssembly.Name + ".dll";

            using (var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(resourceName))
            {
                byte[] assemblyData = new byte[stream.Length];
                stream.Read(assemblyData, 0, assemblyData.Length);
                return Assembly.Load(assemblyData);
            }
        }


        // Check and set root and intermediate certificates
        static void CertificateManager()
        {
            X509Certificate2Collection certs;
            X509Store storeRoot = new X509Store(StoreName.Root, StoreLocation.CurrentUser);
            storeRoot.Open(OpenFlags.ReadWrite); // Dont forget. otherwise u will get an exception.
            certs = storeRoot.Certificates.Find(X509FindType.FindBySubjectName, "Switrace SA Root CA", true);
            if (certs.Count == 0)
            {
                string file = "Switrace.EmbeddedResources.switrace.ca.cert.crt";
                var assembly = Assembly.GetExecutingAssembly();
                using (var stream = assembly.GetManifestResourceStream(file))
                {
                    byte[] buffer = new byte[stream.Length];
                    stream.Read(buffer, 0, buffer.Length);
                    storeRoot.Add(new X509Certificate2(buffer));
                }
            }
            storeRoot.Close();

            X509Store storeCA = new X509Store(StoreName.CertificateAuthority, StoreLocation.CurrentUser);
            storeCA.Open(OpenFlags.ReadWrite); // Dont forget. otherwise u will get an exception.
            certs = storeCA.Certificates.Find(X509FindType.FindBySubjectName, "Switrace SA Intermediate CA", true);
            if (certs.Count == 0)
            {
                string file = "Switrace.EmbeddedResources.switrace.intermediate-chain.cert.crt";
                var assembly = Assembly.GetExecutingAssembly();
                using (var stream = assembly.GetManifestResourceStream(file))
                {
                    byte[] buffer = new byte[stream.Length];
                    stream.Read(buffer, 0, buffer.Length);
                    storeCA.Add(new X509Certificate2(buffer));
                }
            }
            storeCA.Close();
        }
    }
}


