﻿using System;
using System.Dynamic;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fleck;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Reflection;
using System.IO;

namespace Switrace
{
    class WSServer
    {
        public WebSocketServer socket = null;
        Device datalogger = null;
        IWebSocketConnection connection;

        private bool serverStarted = false;

        private string _wsServerLocation = "wss://127.0.0.1:0";
        private int _wsServerPort = 443;
        private string _x509certPath = "Switrace.EmbeddedResources.127.0.0.1.06-20.pfx";
        private string _certPwd = "";


        string clientPublicKey = null;

        public WSServer()
        {

        }

        public bool ServerStarted
        {
            get
            {
                return this.serverStarted;
            }
        }

        private void PublicKey_Received(WSRequestFromClient req)
        {
            WSMessageToClient msg = new WSMessageToClient("publickeyReceived");
            JObject data = JObject.Parse(req.Data);
            this.clientPublicKey = (string)data["publicKey"];

            Security.SetClientPublicKey(this.clientPublicKey);

            this.SendMessage(msg);
        }



        private void SendAck(WSRequestFromClient req)
        {
            WSMessageToClient msg = new WSMessageToClient("Ack");
            msg.RequestId = req.Id;
            this.SendMessage(msg);
        }

        private void Send_DeviceStatus(WSRequestFromClient req)
        {
            WSMessageToClient msg = new WSMessageToClient("deviceStatus");
            if (this.datalogger != null)
            {
                msg.Data = datalogger.SystemStatus.ToString();
            }
                            
            this.SendMessage(msg);            
        }

        private void Send_Stream(WSRequestFromClient req)
        {
            WSMessageToClient msg = new WSMessageToClient("stream");
            if (this.datalogger == null)
            {
                msg.Data = null;
            }
            else
            {
                msg.Data = this.datalogger.OutputForUpload();
            }
            this.SendMessage(msg);       
        }

        private void Send_Refresh(WSRequestFromClient req)
        {
            WSMessageToClient msg = new WSMessageToClient("deviceRefresh");
            if (this.datalogger != null)
            {
                datalogger.Refresh();
            }
            this.SendMessage(msg);
        }

        private void Send_Version(WSRequestFromClient req)
        {
            WSMessageToClient msg = new WSMessageToClient("agentVersion");
            Version agentVersion = Assembly.GetExecutingAssembly().GetName().Version;
            msg.Data = agentVersion.Major + "." + agentVersion.Minor + "." + agentVersion.Build;
            this.SendMessage(msg);
        }

        public void openWebSocket()
        {
            try
            {
                socket = new WebSocketServer(this._wsServerPort, this._wsServerLocation);
                var cert_stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(_x509certPath);
                var cert_bytes = new byte[cert_stream.Length];
                cert_stream.Read(cert_bytes, 0, cert_bytes.Length);
                socket.Certificate = new X509Certificate2(cert_bytes, _certPwd);

                socket.Start(conn =>
                {
                    conn.OnOpen = () =>
                    {
                        this.connection = conn;
                        Console.WriteLine("Connection opened");
                    };

                    conn.OnClose = () =>
                    {
                        this.clientPublicKey = null;
                        Console.WriteLine("Connection closed");
                    };

                    conn.OnMessage = packet =>
                    {
                        JObject jpacket = JObject.Parse(packet);

                        if (jpacket["data"] != null)
                        {
                            Console.WriteLine("Received:" + jpacket["data"]);

                            //byte[] encryptedData = Convert.FromBase64String(jpacket["data"].ToString());
                            //byte[] pwd = Encoding.UTF8.GetBytes("password");
                            string encryptedData = jpacket["data"].ToString();
                            string data = Security.DecodeAES(encryptedData);

                            WSRequestFromClient req = JsonConvert.DeserializeObject<WSRequestFromClient>(data);

                            if (req.Command == "publicKey")
                            {
                                PublicKey_Received(req);
                            }

                            if (req.Command != "Ack")
                            {
                                this.SendAck(req);
                            }

                            if (req.Command == "check")
                            {
                                if (this.datalogger != null)
                                {
                                    this.SendMessage(new WSMessageToClient("deviceAttached"));
                                }
                            }

                            if (req.Command == "deviceStatus")
                            {
                                Send_DeviceStatus(req);
                            }

                            if (req.Command == "deviceRefresh")
                            {
                                Send_Refresh(req);
                                Send_DeviceStatus(req);
                                Send_Stream(req);
                            }

                            if (req.Command == "agentVersion")
                            {
                                Send_Version(req);
                            }

                            if (req.Command == "deviceFirmware")
                            {
                                WSMessageToClient firmware = new WSMessageToClient("deviceFirmware");
                                if (this.datalogger != null)
                                {
                                    firmware.Data = datalogger.FirmwareVersion();
                                }

                                this.SendMessage(firmware);
                            }

                            if (req.Command == "requestData")
                            {
                                WSMessageToClient stream = new WSMessageToClient("stream");
                                if (this.datalogger == null)
                                {
                                    stream.Data = null;
                                }
                                else
                                {
                                    stream.Data = this.datalogger.OutputForUpload();
                                }
                                this.SendMessage(stream);
                            }

                            if (req.Command == "isAttached")
                            {
                                WSMessageToClient message = new WSMessageToClient("stream");
                                if (this.datalogger == null)
                                {
                                    message.Data = null;
                                }
                                else
                                {
                                    message.Data = this.datalogger.OutputForUpload();
                                }
                                this.SendMessage(message);
                            }

                            if (req.Command == "saveSettings")
                            {
                                WSMessageToClient message = new WSMessageToClient("saveSettings");
                                if (this.datalogger == null)
                                {
                                    message.Data = null;
                                }
                                else
                                {
                                    if ((datalogger.SystemStatus != DeviceSystemStates.READY) && (datalogger.Multiuse == 0))
                                    {
                                        message.Error = true;
                                        message.Data = "Cannot reprogram a Single Use device.";
                                    }
                                    else
                                    {
                                        try
                                        {
                                            JObject setts = JObject.Parse(req.Data);
                                            datalogger.MeasuresNumber = Convert.ToUInt16(setts.GetValue("measuresnum"));
                                            datalogger.SamplingTime = Convert.ToUInt16(setts.GetValue("samplingtime"));
                                            datalogger.StartDelay = Convert.ToUInt16(setts.GetValue("startdelay"));
                                            datalogger.CommentFirstRow = (setts.GetValue("firstrow") ?? "").ToString();
                                            datalogger.CommentSecondRow = (setts.GetValue("secondrow") ?? "").ToString();
                                            datalogger.TempMU = (TempMU)Convert.ToUInt16(setts.GetValue("fahrenheit"));
                                            datalogger.AlarmH.SetPoint = Convert.ToSByte(setts.GetValue("alert_h"));
                                            datalogger.AlarmHH.SetPoint = Convert.ToSByte(setts.GetValue("alert_hh"));
                                            datalogger.AlarmL.SetPoint = Convert.ToSByte(setts.GetValue("alert_l"));
                                            datalogger.AlarmLL.SetPoint = Convert.ToSByte(setts.GetValue("alert_ll"));
                                            datalogger.SaveSettings();
                                        }
                                        catch (Exception e)
                                        {
                                            message.Error = true;
                                            Console.WriteLine(e.Message);
                                        }
                                    }
                                }
                                this.SendMessage(message);
                            }
                        }
                    };
                });

                serverStarted = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }


        public void closeWebSocket()
        {
            socket.ListenerSocket.Dispose();
            socket.ListenerSocket.Close();
            socket = null;
            serverStarted = false;
        }



        public bool isConnected()
        {   
            return (socket == null) ? false : true;
        }


        public void loadDatalogger(Device Datalogger = null)
        {
            this.datalogger = Datalogger;
        }


        public void SendMessage(WSMessageToClient message)
        {
            if (connection != null)
            {
                String msg = JsonConvert.SerializeObject(message);
                Console.WriteLine("Sent: " + msg);
                msg = Security.ProtectMessage(msg);

                connection.Send(msg);
            }
        }
    }

    class WSRequestFromClient
    {
        public string Id { get; set;  }
        public string Command { get; set; }
        public string Data { get; set; }
        public bool Error { get; set; }
        public JObject Params { get; }
    }

    class WSMessageToClient
    {
        public WSMessageToClient(string Event = null)
        {
            this.Event = Event;
            this.Id = Guid.NewGuid().ToString();
        }

        public string Id { get; set; }
        public string RequestId { get; set; }
        public string Event { get; set; }
        public string Data { get; set; }
        public bool Error { get; set; }
    }
}
